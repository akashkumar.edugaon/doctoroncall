package com.example.doctoroncall.Fragments

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import com.example.doctoroncall.Activityes.WalletsActivity
import com.example.doctoroncall.Models.SessionManager
import com.example.doctoroncall.R
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import org.json.JSONException
import org.json.JSONObject


class HomeFragment : Fragment(){

    lateinit var wallets:LinearLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view= inflater.inflate(R.layout.fragment_home, container, false)
            wallets = view.findViewById(R.id.wallets_layout)
        wallets.setOnClickListener {
            val intent = Intent(activity,WalletsActivity::class.java)
            startActivity(intent)
        }


       return view
    }


    }



