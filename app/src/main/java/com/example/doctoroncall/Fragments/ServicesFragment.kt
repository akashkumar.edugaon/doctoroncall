package com.example.doctoroncall.Fragments

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.doctoroncall.Activityes.DoctorProfileListActivity
import com.example.doctoroncall.Activityes.SelectLocationActivity
import com.example.doctoroncall.Models.SessionManager
import com.example.doctoroncall.R



class ServicesFragment : Fragment() {

lateinit var hemoDoctor: LinearLayout
lateinit var allopathyDoctor: LinearLayout
lateinit var ayuravedaDoctor: LinearLayout
lateinit var pathology: LinearLayout
lateinit var ambulance: LinearLayout
lateinit var chemist: LinearLayout
lateinit var location: ImageView
lateinit var locationName: TextView
    private var sessionManager: SessionManager? = null
    var distId:String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_services, container, false)
        hemoDoctor = view.findViewById(R.id.homopathy_layout)
        location = view.findViewById(R.id.locationIcon_imageView)
        locationName = view.findViewById(R.id.location_address)
        allopathyDoctor = view.findViewById(R.id.allopathy_doctor)
        ayuravedaDoctor = view.findViewById(R.id.secondRow_layout)
        pathology = view.findViewById(R.id.pathology_layout)
        ambulance = view.findViewById(R.id.thirdRow_layout)
        chemist = view.findViewById(R.id.chemist_layout)

        sessionManager = activity?.let { SessionManager(it.applicationContext) }
        val sharedPreference =  activity?.getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
        val state = sharedPreference?.getString("stateName", "")
        distId = sharedPreference?.getString("distid","").toString()
        var distName = sharedPreference?.getString("distName","")
            if(distName != null){
                locationName.visibility = View.VISIBLE
                locationName.text =distName
            }


        location.setOnClickListener {
            val intent = Intent(activity,SelectLocationActivity::class.java)
            startActivity(intent)
        }
        hemoDoctor.setOnClickListener {
            val intent = Intent(activity,DoctorProfileListActivity::class.java)
            intent.putExtra("id", "4")
            intent.putExtra("distId", distId)
            startActivity(intent)
        }
        allopathyDoctor.setOnClickListener {
            val intent = Intent(activity,DoctorProfileListActivity::class.java)
                intent.putExtra("id","1")
                intent.putExtra("distId",distId)
            startActivity(intent)
        }
        pathology.setOnClickListener {
            val intent = Intent(activity,DoctorProfileListActivity::class.java)
            intent.putExtra("id","2")
            intent.putExtra("distId",distId)
            startActivity(intent)
        }
        ayuravedaDoctor.setOnClickListener {
            val intent = Intent(activity,DoctorProfileListActivity::class.java)
                intent.putExtra("id","5")
                intent.putExtra("distId",distId)
            startActivity(intent)
        }
        ambulance.setOnClickListener {
            val intent = Intent(activity,DoctorProfileListActivity::class.java)
            intent.putExtra("id","3")
            intent.putExtra("distId",distId)
            startActivity(intent)
        }
        chemist.setOnClickListener {
            val intent = Intent(activity,DoctorProfileListActivity::class.java)
            intent.putExtra("id","6")
            intent.putExtra("distId",distId)
            startActivity(intent)
        }

        return view
    }



}