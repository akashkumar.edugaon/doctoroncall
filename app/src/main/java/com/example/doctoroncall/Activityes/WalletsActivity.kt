package com.example.doctoroncall.Activityes

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.doctoroncall.Models.SessionManager
import com.example.doctoroncall.R
import com.google.android.material.textfield.TextInputEditText
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import org.json.JSONObject

class WalletsActivity : AppCompatActivity(), PaymentResultListener {
    lateinit var addAmountBtn: Button
    lateinit var money: TextView
    var sharedpreferences: SharedPreferences? = null
    val SHARED_PREFS = "shared_prefs"
    val KEY_ID = "KEY_ID"
    val KEY_SECRET = "KEY_SECRET"
    var payment_key = ""
    private var transatioId = ""
    var doller = null
    private var totalAmount:Double = 0.00
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallets)
        addAmountBtn = findViewById(R.id.addAmount_button)
        money = findViewById(R.id.amount_textView)


        var sessionManager = SessionManager(this@WalletsActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
         payment_key = sharedpreferences?.getString(KEY_ID,"").toString()
        val secretId = sharedpreferences?.getString(KEY_SECRET,"").toString()

        addAmountBtn.setOnClickListener {
            walletsDialog()
        }
    }

    private fun walletsDialog(){
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.add_amount_item, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
                val payBtn = mDialogView.findViewById<Button>(R.id.add)
                    val amount = mDialogView.findViewById<EditText>(R.id.find_amount)
        val  mAlertDialog = mBuilder.show()
        payBtn.setOnClickListener {
            //get text from EditTexts of custom layout
                    val pay = mDialogView.findViewById<EditText>(R.id.find_amount)
            val u = pay.text.toString().trim()
                val b = u.toInt()
            startPayment(b)
        }
}

    private fun startPayment(b:Int) {
        val checkout = Checkout()
        val amount = Math.round(b.toFloat() * 100)
        val o = checkout.setKeyID(payment_key)
                checkout.setImage(R.drawable.logo)
        val activity = this
        try {
            val options = JSONObject()
            options.put("name", "Doctoroncalls")
            options.put("description", "Wallets charge")
            options.put("currency", "INR")
            options.put("amount", amount)
            val preFill = JSONObject()
            preFill.put("email", "")
            preFill.put("contact", "")
            checkout.open(this,options)
            money.text = amount.toString()
        }
        catch (e:Exception) {
            Toast.makeText(activity, "Error in payment: " + e.message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onPaymentSuccess(transationid: String?) {
                transatioId = transationid.toString()
        Toast.makeText(this,"Payment successfully",Toast.LENGTH_LONG).show()
        if (transationid != null){
            //successfullyDialog()
        }
    }

    override fun onPaymentError(p0: Int, p1: String?) {
        TODO("Not yet implemented")
    }

    private fun successfullyDialog(){
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.payment_successfully, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
        val okButton = findViewById<Button>(R.id.ok_btn)
        val  mAlertDialog = mBuilder.show()
        okButton.setOnClickListener {
           mAlertDialog.dismiss()
        }

    }

}