package com.example.doctoroncall.Activityes

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.FrameLayout
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.doctoroncall.Fragments.HomeFragment
import com.example.doctoroncall.Fragments.OrderListFragment
import com.example.doctoroncall.Fragments.ServicesFragment
import com.example.doctoroncall.Fragments.TransationFragment
import com.example.doctoroncall.Models.SessionManager
import com.example.doctoroncall.R
import com.google.android.material.bottomnavigation.BottomNavigationView


class DashBordActivity : AppCompatActivity() {
    lateinit var frameLayout: FrameLayout
    private var sessionManager: SessionManager? = null
    var drawerLayout: DrawerLayout? = null
    var actionBarDrawerToggle: ActionBarDrawerToggle? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dash_bord)


        // to make the Navigation drawer icon always appear on the action ba
        frameLayout = findViewById(R.id.fragment_frameLayout)
        setupNavigationView()
    }
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        return if (actionBarDrawerToggle!!.onOptionsItemSelected(item)) {
//            true
//        } else super.onOptionsItemSelected(item)
//    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.mymenu, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
                when(id){
                        R.id.termsAndCondition ->{
                            startActivity(Intent(this,TermsAndCondicationActivity::class.java))
                        }
                    R.id.privacyPolicy -> {
                        startActivity(Intent(this,PrivacyPolicyActivity::class.java))
                    }
                    R.id.logOut ->{
                            sessionManager?.logoutUser(this@DashBordActivity)
                        return true

                    }
                }
        return super.onOptionsItemSelected(item)
    }

    private fun setupNavigationView() {
        val bottomNavigationView: BottomNavigationView? = findViewById(R.id.bottom_navigation) as BottomNavigationView?
        if (bottomNavigationView != null) {
            // Select first menu item by default and show Fragment accordingly.
            val menu: Menu = bottomNavigationView.getMenu()
            selectFragment(menu.getItem(0))

            // Set action to perform when any menu-item is selected.
            bottomNavigationView.setOnNavigationItemSelectedListener(
                object : BottomNavigationView.OnNavigationItemSelectedListener {
                    override fun onNavigationItemSelected(item: MenuItem): Boolean {
                        selectFragment(item)
                        return false
                    }
                })
        }
    }

    protected fun selectFragment(item: MenuItem) {
        item.isChecked = true
        when (item.itemId) {
            R.id.home ->                 // Action to perform when Home Menu item is selected.
                pushFragment(HomeFragment())
            R.id.serviceList ->               //  Toast.makeText(this, "work is under progress", Toast.LENGTH_SHORT).show();
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_frameLayout, OrderListFragment(), "loanno").commit()
            R.id.transation ->                 //Toast.makeText(this, "work is under progress", Toast.LENGTH_SHORT).show();
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_frameLayout, TransationFragment()).commit()
            R.id.services ->                 //Toast.makeText(this, "work is under progress", Toast.LENGTH_SHORT).show();
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_frameLayout, ServicesFragment()).commit()
        }
    }
         fun pushFragment(fragment: Fragment?) {
             if (fragment == null) return
             val fragmentManager = fragmentManager
             if (fragmentManager != null) {
                 val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
                 if (transaction != null) {
                     transaction.replace(R.id.fragment_frameLayout, fragment, "aman")
                     transaction.commit()
                 }
             }
         }
}