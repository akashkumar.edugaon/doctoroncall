package com.example.doctoroncall.Activityes

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.ContentValues.TAG
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncall.Models.SessionManager
import com.example.doctoroncall.Models.UrlUtills
import com.example.doctoroncall.R
import com.google.android.material.textfield.TextInputEditText
import de.hdodenhof.circleimageview.CircleImageView
import org.json.JSONArray
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class AddPrescriptionActivity : AppCompatActivity() {
    private var sessionManager: SessionManager? =  null
    lateinit var upload_btn:Button
    lateinit var referalId: TextInputEditText
    lateinit var date: EditText
    lateinit var description: EditText
    private val IMG = 2
    lateinit var uploadImage: ImageView
    lateinit var selectDate: TextView
    lateinit var showImage: CircleImageView
    private var urlUtills: UrlUtills? = UrlUtills()
    var sharedpreferences: SharedPreferences? = null
    val SHARED_PREFS = "shared_prefs"
    val ID = "id"
    private val IMG_REQUEST = 1
    var imageUri: Uri? = null
    var bitmap: Bitmap? = null
    var id = ""
    var refer_id = ""
    var currentDate = ""
    var patientDescription = ""
    var imageURL = ""
    var branchId = ""
    private val calendar = Calendar.getInstance()
    var consultrationFee: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_prescription)

        sessionManager = SessionManager(this@AddPrescriptionActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        id = sharedpreferences?.getString(ID,"").toString()
        Log.e(TAG, "response: " + id)
        branchId = intent.getStringExtra("branch_id").toString()
        consultrationFee = intent.getStringExtra("DOCTOR_CONSULTATION_FEE").toString()
        upload_btn = findViewById(R.id.upload_prescription_btn)
        referalId = findViewById(R.id.mobile_edit_text)
        date = findViewById(R.id.date_editText)
        description = findViewById(R.id.uploadImage_editText)
        showImage = findViewById(R.id.showImage_upload)

        uploadImage = findViewById(R.id.upload_icon)
        selectDate = findViewById(R.id.startDateCalender)

        uploadImage.setOnClickListener {
            selectImage2()
        }

        upload_btn.setOnClickListener {
            validation()
        }
        selectDate.setOnClickListener {
            selectDate()
        }
    }
    private fun validation(){
        refer_id = referalId.text.toString().trim()
        currentDate = selectDate.text.toString().trim()
        patientDescription = description.text.toString().trim()
        imageURL = uploadImage.toString().trim()
        if (refer_id ==""){
            AlertDialog.Builder(this)
                .setMessage("Please enter your Referred By")
                .show()
        } else if (currentDate == ""){
            AlertDialog.Builder(this)
                .setMessage("Please select date")
                .show()
        } else if (patientDescription == ""){
            AlertDialog.Builder(this)
                .setMessage("Please write a description")
                .show()
        }else if (imageURL == ""){
                AlertDialog.Builder(this)
                    .setMessage("Please select image")
                    .show()
        }else{
            UploadPrescription()
        }

    }

    private fun selectDate(){
        val dateSetListener = object : DatePickerDialog.OnDateSetListener{
            override fun onDateSet(view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int){
                calendar.set(Calendar.YEAR,year)
                calendar.set(Calendar.MONTH,monthOfYear)
                calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth)
                userMedicineStartDate()
            }
        }

        DatePickerDialog(this,dateSetListener,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)).show()
    }
    private fun userMedicineStartDate():String {
        val myFormat = "dd-MM-yyyy"
        val sdf = SimpleDateFormat(myFormat,Locale.US)
        selectDate.text = sdf.format(calendar.time)
        return sdf.format(calendar.time)
    }
    private fun selectimage() {
        val i = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(i, IMG_REQUEST)
    }

    private fun selectImage2() {
        val options = arrayOf<CharSequence>("Take Photo", "Choose from Gallery")
        val builder: android.app.AlertDialog.Builder = android.app.AlertDialog.Builder(this)
        builder.setTitle("Add Photo!")
        builder.setItems(options, DialogInterface.OnClickListener { dialog, item ->
            if (options[item] == "Take Photo") {
                selectimage()
            } else if (options[item] == "Choose from Gallery") {
                val intent =
                    Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(intent, 2)
            }
        }).setPositiveButton("Cancel", DialogInterface.OnClickListener { dialog, id -> //TOdo
            dialog.cancel()
        })
        builder.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == IMG_REQUEST && resultCode == RESULT_OK && data != null) {
            val path = data.data
            bitmap = data.extras!!["data"] as Bitmap?
            showImage.setImageBitmap(bitmap)
            showImage.setTag("userimage")
        } else if (requestCode == IMG && resultCode == RESULT_OK && data != null) {
            imageUri = data.data
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri)
                showImage.setImageURI(imageUri)
                showImage.setTag("userimage")
            } catch (e: IOException) {
            }
        } else {
        }
    }

    private fun UploadPrescription(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST, urlUtills?.prescriptionDetails(), Response.Listener { response ->
            Log.e(TAG, "response: " + response)
            try {
                val jsonArray = JSONArray("[$response]")
                val messageObj = jsonArray.getJSONObject(0).get("message")
                val confirmMessageObj = jsonArray.getJSONObject(0).get("confirm_messsage")
                val confirmMessageArray = JSONArray("[$confirmMessageObj]")
                val confirmMessage = confirmMessageArray.getJSONObject(0)
                    val name = confirmMessage.get("BRANCH_NAME")
                    val refer_by = confirmMessage.get("REFERED_BY")
                    val message = confirmMessage.get("MESSAGE")
                    val custoner_file = confirmMessage.get("CUSTOMER_FILE")
                    val patient_name = confirmMessage.get("PATIENT_NAME")
                    val patient_phone = confirmMessage.get("PATIENT_PHONE")

                        sessionManager?.prescriptionDetails(name.toString(),refer_by.toString(), message.toString(),custoner_file.toString(),patient_name.toString(),patient_phone.toString())
                        if (messageObj == "ok"){
                                val intent = Intent(this,AppointmentConfirmActivity::class.java)
                                intent.putExtra("BRANCH_NAME", name.toString())
                                intent.putExtra("REFERED_BY", refer_by.toString())
                                intent.putExtra("MESSAGE", message.toString())
                                intent.putExtra("CUSTOMER_FILE", custoner_file.toString())
                                intent.putExtra("PATIENT_NAME",patient_name.toString())
                                intent.putExtra("PATIENT_PHONE", patient_phone.toString())
                                intent.putExtra("DOCTOR_CONSULTATION_FEE",consultrationFee)
                                    startActivity(intent)
                            finish()
                        }
            } catch (e: Exception) { // caught while parsing the response
                Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                //
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                                params.put("loginid",id)
                                params.put("refered_by", refer_id)
                                params.put("app_date", currentDate)
                                params.put("short_desc", patientDescription)
                                params.put("fileToUpload", "Aksdsf ffd")
                               // bitmap?.let { imagetostring(it)?.let { params.put("fileToUpload", it) } }
                                params.put("appointment_to",branchId)

                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()

                return headers
            }
        }
        // Adding request to request queue
        volleyRequestQueue?.add(strReq)
    }

    private fun imagetostring(bitmap: Bitmap): String? {
        var bitmap = bitmap
        val byteArrayOutputStream = ByteArrayOutputStream()
        val options = BitmapFactory.Options()
        options.inSampleSize = 1
        val scale = resources.displayMetrics.density
        bitmap = Bitmap.createScaledBitmap(bitmap, 600, 550, true)
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
        val imbytes = byteArrayOutputStream.toByteArray()
        return Base64.encodeToString(imbytes, Base64.DEFAULT)
    }
}