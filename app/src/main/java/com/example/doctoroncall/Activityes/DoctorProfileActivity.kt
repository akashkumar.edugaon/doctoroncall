package com.example.doctoroncall.Activityes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncall.Models.Doctor
import com.example.doctoroncall.R
import org.json.JSONArray

class DoctorProfileActivity : AppCompatActivity() {
        lateinit var doctorName: TextView
        lateinit var doctorAddress: TextView
        lateinit var doctorContect: TextView
        lateinit var arrowBack: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doctor_profile)

        doctorName = findViewById(R.id.name)
        doctorAddress = findViewById(R.id.doctor_address)
        doctorContect = findViewById(R.id.doctorcontect)
        arrowBack = findViewById(R.id.arroback)
        arrowBack.setOnClickListener {
            onBackPressed()
        }

      //  DoctorProfileDetails()

        val branchName = intent.getStringExtra("BRANCH_NAME")
        val branchStatus = intent.getStringExtra("profileStatus")
        val pranchContact = intent.getStringExtra("pranchContact")
        val profileStatus = intent.getStringExtra("PROFILE_STATUS")

        doctorName.text = branchName
        doctorAddress.text = branchStatus
        doctorContect.text = pranchContact

    }

    private fun DoctorProfileDetails(){
        var volleyRequestQueue: RequestQueue? = null
        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST, "https://doctoroncalls.in/android/get_my_profile?profileid=1", Response.Listener { response ->
            try {
                val jsonArray = JSONArray("[$response]")
                val messageObj = jsonArray.getJSONObject(0).get("message")
                val messageArray = JSONArray("[$messageObj]")
                val profileObj = messageArray.getJSONObject(0).get("profile") as JSONArray
                profileObj.length()
                for (i in 0 until profileObj.length()){
                    val branchId = profileObj.getJSONObject(i).getString("BRANCH_ID")
                    val branchName = profileObj.getJSONObject(i).getString("BRANCH_NAME")
                    val profileStatus = profileObj.getJSONObject(i).getString("PROFILE_STATUS")
                    val doctor = Doctor(branchId,"",branchName,"","",profileStatus)
                }

                Log.d("profile", profileObj.toString())

            } catch (e: Exception) { // caught while parsing the response
                Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                //
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                return params
            }

            override fun getHeaders(): Map<String, String> {

                val headers: MutableMap<String, String> = HashMap()
                // Add your Header paramters here
                return headers
            }
        }
        // Adding request to request queue
        volleyRequestQueue?.add(strReq)
    }

}
