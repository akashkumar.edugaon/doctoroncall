package com.example.doctoroncall.Activityes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncall.Adapter.CalendarAdapter
import com.example.doctoroncall.Adapter.OnCalenderItemClickListener
import com.example.doctoroncall.Models.Doctor
import com.example.doctoroncall.Models.DoctorModels
import com.example.doctoroncall.R
import org.json.JSONArray
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class
BookAppointmentActivity : AppCompatActivity(){
    lateinit var doctorName: TextView
    lateinit var consulatinFee: TextView
    lateinit var calenderRecyclerView: RecyclerView
    lateinit var currentMonthTextView: TextView
    lateinit var selectDate: TextView
    lateinit var nextWeek: TextView
    private var doctorModels: DoctorModels? = DoctorModels()
    lateinit var patientMobile: EditText
    lateinit var patientName: EditText
    lateinit var patientAddress: EditText
    lateinit var appointmentButton: Button
    var name = ""
    var mobile = ""
    var address = ""
    var branchId = ""
    private val dates = ArrayList<Date>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_appointment)

        patientMobile = findViewById(R.id.mobileNumber)
        patientName = findViewById(R.id.patientName)
        patientAddress = findViewById(R.id.address)
        appointmentButton = findViewById(R.id.appointmentName)


        doctorName = findViewById(R.id.doctorName)
        consulatinFee = findViewById(R.id.rupays_title)

        val branchName = intent.getStringExtra("BRANCH_NAME")
        val branchStatus = intent.getStringExtra("profileStatus")
        val pranchContact = intent.getStringExtra("pranchContact")
        val profileStatus = intent.getStringExtra("PROFILE_STATUS")
        val consultationFees = intent.getStringExtra("consultationFees")
        branchId = intent.getStringExtra("branchId").toString()

        // branchId = doctorModels?.doctor?.BRANCH_ID.toString().trim()

        doctorName.text = branchName
        consulatinFee.text = consultationFees

        appointmentButton.setOnClickListener {
            mobile = patientMobile.text.toString().trim()
            name = patientName.text.toString().trim()
            address = patientAddress.text.toString().trim()
            if (mobile == "") {
                AlertDialog.Builder(this)
                    .setMessage("Please enter your mobile")
                    .show()
            } else if (name == "") {
                AlertDialog.Builder(this)
                    .setMessage("Please enter your name")
                    .show()
            } else if (address == "") {
                AlertDialog.Builder(this)
                    .setMessage("Please enter your Address")
            } else {
                bookAppointment()
            }
        }
    }


    private fun bookAppointment(){
            var volleyRequestQueue: RequestQueue? = null
            val TAG = "Handy Opinion Tutorials"

            volleyRequestQueue = Volley.newRequestQueue(this)
            val strReq: StringRequest = object : StringRequest(Method.POST, "https://doctoroncalls.in/android/add_appointment", Response.Listener { response ->
                Log.e(TAG, "response: " + response)
                try {
                    val jsonArray = JSONArray("[$response]")
                    val messageObj = jsonArray.getJSONObject(0).get("appoint_detail")
                    val message = jsonArray.getJSONObject(0).get("message")
                    val messageArray = JSONArray("[$messageObj]")
                        val da_id = messageArray.getJSONObject(0).getString("DA_ID")
                            print(da_id)
                            if(message == "sucess"){
                                val intent = Intent(this,AppointmentConfirmActivity::class.java)
                                    intent.putExtra("daId", da_id)
                                startActivity(intent)
                            }


                } catch (e: Exception) { // caught while parsing the response
                    Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                    e.printStackTrace()
                }
            },
                Response.ErrorListener { volleyError -> // error occurred
                    //
                    Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
                }) {

                override fun getParams(): MutableMap<String, String> {
                    val params: MutableMap<String, String> = java.util.HashMap()
                            params.put("patient_name", name)
                            params.put("patient_mobile", mobile)
                            params.put("patient_address", address)
                            params.put("apointment_date", "26-02-2022")
                            params.put("apointment_slot", "2")
                            params.put("appointment_type","2")
                            params.put("doctorid", branchId)
                            params.put("loginid", "1")
                    return params
                }

                override fun getHeaders(): Map<String, String> {

                    val headers: MutableMap<String, String> = HashMap()
                    // Add your Header paramters here
                    return headers
                }
            }
            // Adding request to request queue
            volleyRequestQueue?.add(strReq)
        }
    }
