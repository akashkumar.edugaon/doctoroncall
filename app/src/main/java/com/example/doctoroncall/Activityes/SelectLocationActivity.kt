package com.example.doctoroncall.Activityes

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncall.Models.*
import com.example.doctoroncall.R
import org.json.JSONArray


class SelectLocationActivity : AppCompatActivity() {
    lateinit var selectState: Spinner
    lateinit var selectDist: Spinner
    lateinit var submitButton: Button
    lateinit var stateNameT: TextView
    lateinit var arrowBack: ImageView
    private var urlUtills: UrlUtills? = UrlUtills()
    var stateId = ""
    var distId = ""
    var stateName = ""
    var id = ""
    var states = java.util.HashMap<String, String>()

    private var stateList = ArrayList<String>()
    private var allStateList = ArrayList<State>()
    private val distList = ArrayList<String>()
    private val allDistList = ArrayList<Dist>()
    private var sessionManager: SessionManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_location)
        selectState = findViewById(R.id.state_list)
        selectDist = findViewById(R.id.select_dist)
        submitButton = findViewById(R.id.submit_btn)
        arrowBack = findViewById(R.id.arrowBack)

        sessionManager = SessionManager(this@SelectLocationActivity)

        id = intent.getStringExtra("id").toString()

        arrowBack.setOnClickListener {
            onBackPressed()
        }

            selectState()

        submitButton.setOnClickListener {
//            val intent = Intent(this,DoctorProfileListActivity::class.java)
//            startActivity(intent)
            onBackPressed()
        }

        selectState.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                stateId = allStateList[p2].STATE_ID.toString()
                 stateName = allStateList[p2].STATE_NAME.toString()
                sessionManager?.locationDetails(stateName.toString(),"","","")
                selectDist()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        selectDist.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
               val distId = allDistList[p2].DISTRICT_ID.toString()
                val distName = allDistList[p2].DISTRICT_NAME.toString()

                sessionManager?.locationDetails("","",distId,distName.toString())

                val sharedPreference =  getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
                val editor = sharedPreference.edit()
                editor.putString("distid",distId)
                editor.putString("stateName",stateName)
                editor.putString("distName",distName)
                editor.putLong("l",100L)
                editor.apply()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }
//        spinnervalue()
    }

    private fun selectState(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST,urlUtills?.stateApi(), Response.Listener { response ->
            Log.e(TAG, "response: " + response)
            try {
                val jsonArray = JSONArray("[$response]")
                val messageObj = jsonArray.getJSONObject(0).get("STATE") as JSONArray
                        for (i in 0 until messageObj.length()) {
                            val stateId = messageObj.getJSONObject(i).getString("STATE_ID")
                            val stateName = messageObj.getJSONObject(i).getString("STATE_NAME")
                            val countryId = messageObj.getJSONObject(i).getString("COUNTRY_ID")
                            val state = State(stateId,stateName,countryId)
                            allStateList.add(state)
                            stateList.add(stateName)
                        }
                showStateList()
                Log.d("profile", stateList.toString())

            } catch (e: Exception) { // caught while parsing the response
                Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                //
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                return params
            }
            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
    }

    private fun showStateList(){
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, stateList)
        selectState.adapter = adapter
    }


    private fun selectDist(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST,urlUtills?.distApi() , Response.Listener { response ->
            Log.e(TAG, "response: " + response)
            try {
                val jsonArray = JSONArray("[$response]")
                val messageObj = jsonArray.getJSONObject(0).get("DISTRICT") as JSONArray
                val item = messageObj.length()
                if (!distList.isEmpty()) distList.clear()
                if (!allDistList.isEmpty()) allDistList.clear()
                for (i in 0 until messageObj.length()) {
                    var distId = messageObj.getJSONObject(i).getString("DISTRICT_ID")
                    var distName = messageObj.getJSONObject(i).getString("DISTRICT_NAME")
                    val dist = Dist(distId,distName)
                    allDistList.add(dist)
                    distList.add(distName)
                    showDistList()
                }


            } catch (e: Exception) { // caught while parsing the response
                Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                //
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("stateid", stateId)
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                // Add your Header paramters here
                return headers
            }
        }
        // Adding request to request queue
        volleyRequestQueue?.add(strReq)
    }

    private fun showDistList(){
//        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, distList)
//        selectDist.adapter = adapter

        val dataAdapter = ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item,distList)
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        selectDist.adapter = dataAdapter
    }

}


