package com.example.doctoroncall.Activityes

import android.Manifest
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncall.Adapter.DoctorAdapter
import com.example.doctoroncall.Models.Doctor
import com.example.doctoroncall.Models.DoctorModels
import com.example.doctoroncall.Models.SessionManager
import com.example.doctoroncall.Models.UrlUtills
import com.example.doctoroncall.R
import org.json.JSONArray

class DoctorProfileListActivity : AppCompatActivity(), DoctorAdapter.OnItemClickListener {
    lateinit var doctorListRecycler: RecyclerView
    lateinit var dataNotAvailabele: TextView
    lateinit var viewAppointment: TextView
    lateinit var ambulanceList: TextView
    private var doctorModels: DoctorModels? = DoctorModels()
    private var doctorList = ArrayList<Doctor>()
    private var urlUtills: UrlUtills? = UrlUtills()
    private var PRIVATE_MODE = 0
    private val PREF_NAME = "mindorks-welcome"
    private var sessionManager: SessionManager? = null
    val SHARED_PREFS = "shared_prefs"
    val KEY_STATE_NAME = "KEY_STATE_NAME"
    var sharedpreferences: SharedPreferences? = null
    var id: String? = ""
    var distId: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doctor_profile_list)

        sessionManager = SessionManager(this@DoctorProfileListActivity)
        doctorListRecycler= findViewById(R.id.doctorList_recyclerView)
        dataNotAvailabele = findViewById(R.id.dataNotAvailabe_title)
        ambulanceList = findViewById(R.id.amulance_title)
        val back = findViewById<ImageView>(R.id.arroback)

        back.setOnClickListener {
            onBackPressed()
        }
        id = intent.getStringExtra("id").toString()
        distId = intent.getStringExtra("distId").toString()

        if (id == "3"){
            val ambulanceListT = "Ambulance Lists"
            ambulanceList.text = ambulanceListT
        } else if (id == "6") {
            val chemust = "Chemist Lists"
            ambulanceList.text = chemust
        }
        else{

        }
        getPreToken()
    }

    private fun getPreToken(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST, urlUtills?.doctorDetailsListAPi()+"$id", Response.Listener { response ->
            Log.e(TAG, "response: " + response)
            try {
                val jsonArray = JSONArray("[$response]")
                val messageObj = jsonArray.getJSONObject(0).get("message")
                val messageArray = JSONArray("[$messageObj]")
                val profileObj = messageArray.getJSONObject(0).get("profile") as JSONArray
                profileObj.length()
                for (i in 0 until profileObj.length()){
                            val branchId = profileObj.getJSONObject(i).getString("BRANCH_ID")
                            val branchName = profileObj.getJSONObject(i).getString("BRANCH_NAME")
                            val profileStatus = profileObj.getJSONObject(i).getString("PROFILE_STATUS")
                            val branch_cont = profileObj.getJSONObject(i).getString("BRANCH_CONTACT")
                            val experience = profileObj.getJSONObject(i).getString("EXPERIENCE")
                            val branchEmail = profileObj.getJSONObject(i).getString("BRANCH_EMAIL")
                            val consultationFees = profileObj.getJSONObject(i).getString("DOCTOR_CONSULTATION_FEE")
                            val firmId = profileObj.getJSONObject(i).getString("FIRM_ID")
                            val branchCode = profileObj.getJSONObject(i).getString("BRANCH_CODE")


                    val doctor = Doctor(branchId,firmId,branchName,branchCode,branch_cont,profileStatus,consultationFees,branchEmail,experience)

                    doctorList.add(doctor)
                }
                if (doctorList != null){
                    showList(doctorList)
                }else{
                    doctorListRecycler.visibility = View.VISIBLE
                    doctorListRecycler.visibility = View.GONE
                }
                Log.d("profile", profileObj.toString())

            } catch (e: Exception) { // caught while parsing the response
                Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                //
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("districtId",distId.toString())
                return params
            }

            override fun getHeaders(): Map<String, String> {

                val headers: MutableMap<String, String> = HashMap()
                // Add your Header paramters here
                return headers
            }
        }
        // Adding request to request queue
        volleyRequestQueue?.add(strReq)
    }

private fun showList(doctorList: List<Doctor>){

     val doctorAdapter = DoctorAdapter(doctorList, this)
    val layoutManage = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
            doctorListRecycler.layoutManager = layoutManage
            doctorListRecycler.adapter = doctorAdapter
}

//    override fun onclick(doctor: Doctor) {
//
//    }

    override fun viewButton(doctor: Doctor) {
        val intent = Intent(this,DoctorProfileActivity::class.java)
                    intent.putExtra("PROFILE_STATUS", doctor.PROFILE_STATUS)
                    intent.putExtra("BRANCH_NAME", doctor.BRANCH_NAME)
                    intent.putExtra("profileStatus", doctor.PROFILE_STATUS)
                    intent.putExtra("pranchContact", doctor.BRANCH_CONTACT)
                    intent.putExtra("consultationFees", doctor.DOCTOR_CONSULTATION_FEE)
                        startActivity(intent)
    }

    override fun bookAppointment(doctor: Doctor) {
        val intent = Intent(this,BookAppointmentActivity::class.java)
        intent.putExtra("PROFILE_STATUS", doctor.PROFILE_STATUS)
        intent.putExtra("BRANCH_NAME", doctor.BRANCH_NAME)
        intent.putExtra("profileStatus", doctor.PROFILE_STATUS)
        intent.putExtra("pranchContact", doctor.BRANCH_CONTACT)
        intent.putExtra("consultationFees", doctor.DOCTOR_CONSULTATION_FEE)
        intent.putExtra("branchId", doctor.BRANCH_ID)
        startActivity(intent)
    }

    override fun addPrescription(doctor: Doctor) {
        val intent = Intent(this,AddPrescriptionActivity::class.java)
            intent.putExtra("branch_id", doctor.BRANCH_ID)
            intent.putExtra("DOCTOR_CONSULTATION_FEE", doctor.DOCTOR_CONSULTATION_FEE)
        startActivity(intent)
    }

    override fun callToAmbulance(doctor: Doctor) {
        makeCall(doctor.BRANCH_CONTACT.toString())
    }

    private fun makeCall(number: String){
        val intent = Intent(Intent.ACTION_CALL)
        intent.data = Uri.parse("tel:$number")
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED
        ) {

            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE), 100)
           // Toast.makeText(this,"Permission denied",Toast.LENGTH_LONG).show()
            return
        }
        startActivity(intent)
    }
}