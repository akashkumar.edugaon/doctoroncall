package com.example.doctoroncall.Activityes

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncall.Models.SessionManager
import com.example.doctoroncall.Models.UrlUtills
import com.example.doctoroncall.R
import com.google.android.material.textfield.TextInputEditText
import org.json.JSONArray

class RegisterActivityActivity : AppCompatActivity() {
    lateinit var register_btn: Button
    lateinit var userName: TextInputEditText
    lateinit var userMobile: TextInputEditText
    lateinit var userPass: TextInputEditText
    private var urlUtills: UrlUtills? = UrlUtills()
    private var sessionManager: SessionManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_activity)

        sessionManager = SessionManager(this@RegisterActivityActivity)
        register_btn = findViewById(R.id.register_btn)
        userName = findViewById(R.id.name_edit_text)
        userMobile = findViewById(R.id.mobile_number)
        userPass = findViewById(R.id.pass_text)

        register_btn.setOnClickListener {
            register()

        }
    }
    private fun register(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST,urlUtills?.register(), Response.Listener { response ->
            Log.e(TAG, "response: " + response)
            try {
                val jsonArray = JSONArray("[$response]")
                val jsonObject = jsonArray.getJSONObject(0)
                val message = jsonObject.getString("message")
                if (message == "already"){
                        Toast.makeText(this,"mobile number already register",Toast.LENGTH_LONG).show()
                } else if (message == "ok"){
                    val id = jsonObject.getString("id")
                    val key_id = jsonObject.getString("KEY_ID")
                    val name = jsonObject.getString("name")
                    val contact = jsonObject.getString("contact")
                    val Key_Secret = jsonObject.getString("KEY_SECRET")
                    val token = jsonObject.getString("token")
                            sessionManager?.createRegisterSession(id,key_id,name,contact,token,Key_Secret)
                    val intent= Intent(this,DashBordActivity::class.java)
                    // Closing all the Activities
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                    finish()
                }
            } catch (e: Exception) { // caught while parsing the response
                Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                //
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("name", userName.text.toString().trim())
                params.put("contact", userMobile.text.toString().trim())
                params.put("password", userPass.text.toString().trim())
                params.put("referal_code", "111")
                return params
            }

            override fun getHeaders(): Map<String, String> {

                val headers: MutableMap<String, String> = HashMap()
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
    }

}