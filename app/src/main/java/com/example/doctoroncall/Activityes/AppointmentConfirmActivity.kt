package com.example.doctoroncall.Activityes

import android.content.ContentValues
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncall.R
import com.google.android.material.tabs.TabLayout
import org.json.JSONArray
import java.util.HashMap

class AppointmentConfirmActivity : AppCompatActivity() {

    lateinit var doctorName: TextView
    lateinit var patientName: TextView
    lateinit var patientNumber: TextView
    lateinit var appointmentTime: TextView
    lateinit var doctorFees: TextView
    lateinit var confirmAppointment: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_appointment_confirm)
        val bookigid = findViewById<TextView>(R.id.bookingId)
        doctorName = findViewById(R.id.doctorName)
        patientName = findViewById(R.id.patientName_title)
        patientNumber = findViewById(R.id.mobileNumber)
        appointmentTime = findViewById(R.id.appointment_date)
        doctorFees = findViewById(R.id.doctorsFees)
        confirmAppointment = findViewById(R.id.appointmentName)

        val patientNmae = intent.getStringExtra("patientName")
        val patientAddress = intent.getStringExtra("patientAddress")
        val bookingId = intent.getStringExtra("bookingId")
        bookigid.text = bookingId

        val naem = intent.getStringExtra("BRANCH_NAME")
        val refer = intent.getStringArrayListExtra("REFERED_BY")
        val message = intent.getSerializableExtra("MESSAGE")
        val patientNam = intent.getStringExtra("PATIENT_NAME")
        val patienyNu = intent.getStringExtra("PATIENT_PHONE")
        val cosutltationFee = intent.getStringExtra("DOCTOR_CONSULTATION_FEE")
        doctorName.text = naem
        patientName.text = patientNmae
        patientNumber.text = patienyNu
      //  doctorFees.text = "Consultation fees "+cosutltationFee+"-"

        confirmAppointment.setOnClickListener {
            val intent = Intent(this,DashBordActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }
    }

    }
