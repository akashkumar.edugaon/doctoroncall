package com.example.doctoroncall.Models

class UrlUtills {
    var mainUrl = "https://doctoroncalls.in/android/"

    fun register(): String{
        var url = mainUrl+"customer_signup"
            return url
    }
    fun login(): String{
        var url = mainUrl+"/user_login"
        return url
    }
    fun stateApi(): String{
        val url = mainUrl+"/get_states"
        return url
    }
    fun distApi(): String{
        val url = mainUrl+"/get_district"
        return url
    }
    fun doctorDetailsListAPi():String{
        val url = mainUrl+"get_profile_by_status?status="
        return url
    }
    fun prescriptionDetails(): String{
        val url = mainUrl+"uploads_patient_prescription"
            return url
    }
}