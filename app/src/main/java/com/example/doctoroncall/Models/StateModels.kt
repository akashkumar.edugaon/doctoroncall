package com.example.doctoroncall.Models

class StateModels {
        var state: State? = State()
}

data class State(
    var STATE_ID: String? = null,
    var STATE_NAME: String? = null,
    var COUNTRY_ID: String? = null
)

data class Dist(
    var DISTRICT_ID: String? = null,
    var DISTRICT_NAME: String? = null
)
