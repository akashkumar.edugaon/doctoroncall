package com.example.doctoroncall.Models

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.example.doctoroncall.Activityes.LoginActivity
import com.example.doctoroncall.Fragments.ServicesFragment


class SessionManager(val context: Context) {
    var pref = context.getSharedPreferences("shared_prefs" , Context.MODE_PRIVATE)
    var editor = pref.edit()
    var _context: Context? = null
    var PRIVATE_MODE = 0
    private val PREF_NAME = "CTL"

    val IS_USER_LOGIN = "IsUserLoggedIn"

    val KEY_CODE = "code"
    val ID = "id"
    val KEY_ID = "KEY_ID"
    val KEY_MOBILE = "mobile"
    val KEY_NAME = "KEY_NAME"
    val KEY_CONTACT = "KEY_NAME"
    val KEY_SECRET = "KEY_SECRET"
    val KEY_TOKEN = "KEY_TOKEN"

    //location details
        val KEY_STATE_NAME = "KEY_STATE_NAME"
        val KEY_STATE_ID = "KEY_STATE_ID"
        val KEY_DIST_ID = "KEY_DIST_ID"
        val KEY_DIST_NAME = "KEY_DIST_NAME"

    // mobile verfication
    var VERIFICATION_MOBILE = "KEY_MOBILE"
    var VERIF_ID = "VERIFICATION_ID"

        // prescription details
        val BRANCH_NAME = "BRANCH_NAME"
        val REFERED_BY = "REFERED_BY"
        val MESSAGE = "MESSAGE"
        val CUSTOMER_FILE = "CUSTOMER_FILE"
        val PATIENT_NAME = "PATIENT_NAME"
        val PATIENT_PHONE = "PATIENT_PHONE"

    fun createMobileVerification(verificationMobile:String, verficationId:String){
        editor?.putString(VERIFICATION_MOBILE,verificationMobile)
        editor?.putString(VERIF_ID, verficationId)
        editor?.commit()

    }

    fun createRegisterSession(id: String?, key_id: String, name: String, contact: String?,token:String,key_secret:String) {
        editor?.putBoolean(IS_USER_LOGIN, true)
        editor?.putString(ID, id)
        editor?.putString(KEY_ID, key_id)
        editor?.putString(KEY_NAME, name)
        editor?.putString(KEY_CONTACT, contact)
        editor?.putString(KEY_SECRET, key_secret)
        editor?.putString(KEY_TOKEN,token)
        editor?.commit()
    }

    fun locationDetails(stateName:String, stateId:String,distId:String, distName:String){
        editor?.putString(KEY_STATE_NAME,stateName)
        editor?.putString(KEY_STATE_ID, stateId)
        editor?.putString(KEY_DIST_ID, distId)
        editor?.putString(KEY_DIST_NAME, distName)
        editor?.commit()
    }
    fun prescriptionDetails(branchName:String,refered_by:String,message:String,customer_file:String, patientName:String,patientPhone:String) {

    }

    fun logoutUser(main_context : Context){
        _context=main_context
        editor?.clear()?.apply()
       // editor?.commit()
        val intent = Intent(_context,LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        _context?.startActivity(intent)
    }

}
