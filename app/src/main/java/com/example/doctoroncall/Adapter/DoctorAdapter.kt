package com.example.doctoroncall.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.doctoroncall.Models.Doctor
import com.example.doctoroncall.R

class DoctorAdapter(var doctorList: List<Doctor>, private var onItemClickListener: OnItemClickListener ): RecyclerView.Adapter<DoctorAdapter.DoctorViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DoctorAdapter.DoctorViewHolder {
       return DoctorViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.doctor_list_item, parent, false), onItemClickListener)
    }

    override fun onBindViewHolder(holder: DoctorAdapter.DoctorViewHolder, position: Int) {
            holder.bind(doctorList[position])
        val data =doctorList[position]
            holder.doctorList = data
    }

    override fun getItemCount(): Int {
      return doctorList.size
    }
    class DoctorViewHolder(itemView: View, onItemClickListener: OnItemClickListener): RecyclerView.ViewHolder(itemView){
        lateinit var doctorList: Doctor
        var prifleStatus:String = ""
            fun bind(doctor: Doctor){
                        val name = itemView.findViewById<TextView>(R.id.name_listView)
                        val experience = itemView.findViewById<TextView>(R.id.adress_titile)
                        val viewButton = itemView.findViewById<Button>(R.id.view_profile)
                        val bookAppointment = itemView.findViewById<Button>(R.id.bookAppointment)
                        val collNow = itemView.findViewById<Button>(R.id.CollNow_button)
                        val addPrescription = itemView.findViewById<Button>(R.id.addPrescription_button)
                    name.text = doctor.BRANCH_NAME
                    experience.text = doctor.EXPERIENCE
                prifleStatus = doctor.PROFILE_STATUS.toString()

                if (prifleStatus == "6"){
                    bookAppointment.visibility = View.GONE
                    collNow.visibility = View.GONE
                    addPrescription.visibility = View.VISIBLE
                } else if (prifleStatus == "2"){
                    bookAppointment.visibility = View.GONE
                    collNow.visibility = View.GONE
                    addPrescription.visibility = View.VISIBLE
                }else{
                    bookAppointment.visibility = View.VISIBLE
                    collNow.visibility = View.GONE
                    addPrescription.visibility = View.GONE
                }
                if(prifleStatus == "3"){
                    bookAppointment.visibility = View.GONE
                    collNow.visibility = View.VISIBLE
                }else{
                    bookAppointment.visibility = View.VISIBLE
                    collNow.visibility = View.GONE
                }
            }
        init {
            val viewButton = itemView.findViewById<Button>(R.id.view_profile)
                viewButton.setOnClickListener {
                onItemClickListener.viewButton(doctorList)
            }
            val callNow = itemView.findViewById<Button>(R.id.CollNow_button)
            callNow.setOnClickListener {
                onItemClickListener.callToAmbulance(doctorList)
            }
            val bookAppointment = itemView.findViewById<Button>(R.id.bookAppointment)
            bookAppointment.setOnClickListener {
                onItemClickListener.bookAppointment(doctorList)
            }
            val addPrescription = itemView.findViewById<Button>(R.id.addPrescription_button)
                addPrescription.setOnClickListener {
                    onItemClickListener.addPrescription(doctorList)
                }
        }
    }
    interface OnItemClickListener{
        fun viewButton(doctor: Doctor)
        fun bookAppointment(doctor: Doctor)
        fun addPrescription(doctor: Doctor)
        fun callToAmbulance(doctor: Doctor)
    }
}


