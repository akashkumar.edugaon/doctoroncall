package com.example.doctoroncall.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.doctoroncall.Models.Doctor
import com.example.doctoroncall.Models.State
import com.example.doctoroncall.R


class StateListAdapter(var stateList: List<State>): RecyclerView.Adapter<StateListAdapter.StateViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int, ): StateViewHolder{
                return StateViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.simple_list_item,parent, false))
    }

    override fun onBindViewHolder(holder: StateListAdapter.StateViewHolder, position: Int) {
       holder.bind(stateList[position])
    }

    override fun getItemCount(): Int {
       return stateList.size
    }
    class StateViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
                fun bind(state: State){


                }
    }
}